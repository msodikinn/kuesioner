<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'rpm1' => [
            'required' => 'Pertanyaan No 1 bagian minimum harus di isi',
        ],
		'rpd1' => [
            'required' => 'Pertanyaan No 1 bagian desired harus di isi',
        ],
		'rpp1' => [
            'required' => 'Pertanyaan No 1 bagian perceived harus di isi',
        ],
		'rpm2' => [
            'required' => 'Pertanyaan No 2 bagian minimum harus di isi',
        ],
		'rpd2' => [
            'required' => 'Pertanyaan No 2 bagian desired harus di isi',
        ],
		'rpp2' => [
            'required' => 'Pertanyaan No 2 bagian perceived harus di isi',
        ],
		        'rpm3' => [
            'required' => 'Pertanyaan No 3 bagian minimum harus di isi',
        ],
		'rpd3' => [
            'required' => 'Pertanyaan No 3 bagian desired harus di isi',
        ],
		'rpp3' => [
            'required' => 'Pertanyaan No 3 bagian perceived harus di isi',
        ],
		
		        'rpm4' => [
            'required' => 'Pertanyaan No 4 bagian minimum harus di isi',
        ],
		'rpd4' => [
            'required' => 'Pertanyaan No 4 bagian desired harus di isi',
        ],
		'rpp4' => [
            'required' => 'Pertanyaan No 4 bagian perceived harus di isi',
        ],
		        'rpm5' => [
            'required' => 'Pertanyaan No 5 bagian minimum harus di isi',
        ],
		'rpd5' => [
            'required' => 'Pertanyaan No 5 bagian desired harus di isi',
        ],
		'rpp5' => [
            'required' => 'Pertanyaan No 5 bagian perceived harus di isi',
        ],
		        'rpm6' => [
            'required' => 'Pertanyaan No 6 bagian minimum harus di isi',
        ],
		'rpd6' => [
            'required' => 'Pertanyaan No 6 bagian desired harus di isi',
        ],
		'rpp6' => [
            'required' => 'Pertanyaan No 6 bagian perceived harus di isi',
        ],
		
		        'rpm7' => [
            'required' => 'Pertanyaan No 7 bagian minimum harus di isi',
        ],
		'rpd7' => [
            'required' => 'Pertanyaan No 7 bagian desired harus di isi',
        ],
		'rpp7' => [
            'required' => 'Pertanyaan No 7 bagian perceived harus di isi',
        ],
		
		        'rpm8' => [
            'required' => 'Pertanyaan No 8 bagian minimum harus di isi',
        ],
		'rpd8' => [
            'required' => 'Pertanyaan No 8 bagian desired harus di isi',
        ],
		'rpp8' => [
            'required' => 'Pertanyaan No 8 bagian perceived harus di isi',
        ],
		
		        'rpm9' => [
            'required' => 'Pertanyaan No 9 bagian minimum harus di isi',
        ],
		'rpd9' => [
            'required' => 'Pertanyaan No 9 bagian desired harus di isi',
        ],
		'rpp9' => [
            'required' => 'Pertanyaan No 9 bagian perceived harus di isi',
        ],
		
		'rpm10' => [
            'required' => 'Pertanyaan No 10 bagian minimum harus di isi',
        ],
		'rpd10' => [
            'required' => 'Pertanyaan No 10 bagian desired harus di isi',
        ],
		'rpp10' => [
            'required' => 'Pertanyaan No 10 bagian perceived harus di isi',
        ],
		
		        'rpm11' => [
            'required' => 'Pertanyaan No 11 bagian minimum harus di isi',
        ],
		'rpd11' => [
            'required' => 'Pertanyaan No 11 bagian desired harus di isi',
        ],
		'rpp11' => [
            'required' => 'Pertanyaan No 11 bagian perceived harus di isi',
        ],
		
		        'rpm12' => [
            'required' => 'Pertanyaan No 12 bagian minimum harus di isi',
        ],
		'rpd12' => [
            'required' => 'Pertanyaan No 12 bagian desired harus di isi',
        ],
		'rpp12' => [
            'required' => 'Pertanyaan No 12 bagian perceived harus di isi',
        ],
		
		        'rpm13' => [
            'required' => 'Pertanyaan No 13 bagian minimum harus di isi',
        ],
		'rpd13' => [
            'required' => 'Pertanyaan No 13 bagian desired harus di isi',
        ],
		'rpp13' => [
            'required' => 'Pertanyaan No 13 bagian perceived harus di isi',
        ],
		
		        'rpm14' => [
            'required' => 'Pertanyaan No 14 bagian minimum harus di isi',
        ],
		'rpd14' => [
            'required' => 'Pertanyaan No 14 bagian desired harus di isi',
        ],
		'rpp14' => [
            'required' => 'Pertanyaan No 14 bagian perceived harus di isi',
        ],
		        'rpm15' => [
            'required' => 'Pertanyaan No 15 bagian minimum harus di isi',
        ],
		'rpd15' => [
            'required' => 'Pertanyaan No 15 bagian desired harus di isi',
        ],
		'rpp15' => [
            'required' => 'Pertanyaan No 15 bagian perceived harus di isi',
        ],
		        'rpm16' => [
            'required' => 'Pertanyaan No 16 bagian minimum harus di isi',
        ],
		'rpd16' => [
            'required' => 'Pertanyaan No 16 bagian desired harus di isi',
        ],
		'rpp16' => [
            'required' => 'Pertanyaan No 16 bagian perceived harus di isi',
        ],
		
		'rpm17' => [
            'required' => 'Pertanyaan No 17 bagian minimum harus di isi',
        ],
		'rpd17' => [
            'required' => 'Pertanyaan No 17 bagian desired harus di isi',
        ],
		'rpp17' => [
            'required' => 'Pertanyaan No 17 bagian perceived harus di isi',
        ],
		
		        'rpm18' => [
            'required' => 'Pertanyaan No 18 bagian minimum harus di isi',
        ],
		'rpd18' => [
            'required' => 'Pertanyaan No 18 bagian desired harus di isi',
        ],
		'rpp18' => [
            'required' => 'Pertanyaan No 18 bagian perceived harus di isi',
        ],
		
		        'rpm19' => [
            'required' => 'Pertanyaan No 19 bagian minimum harus di isi',
        ],
		'rpd19' => [
            'required' => 'Pertanyaan No 19 bagian desired harus di isi',
        ],
		'rpp19' => [
            'required' => 'Pertanyaan No 19 bagian perceived harus di isi',
        ],
		
		        'rpm20' => [
            'required' => 'Pertanyaan No 20 bagian minimum harus di isi',
        ],
		'rpd20' => [
            'required' => 'Pertanyaan No 20 bagian desired harus di isi',
        ],
		'rpp20' => [
            'required' => 'Pertanyaan No 20 bagian perceived harus di isi',
        ],
		
				        'rpm21' => [
            'required' => 'Pertanyaan No 21 bagian minimum harus di isi',
        ],
		'rpd21' => [
            'required' => 'Pertanyaan No 21 bagian desired harus di isi',
        ],
		'rpp21' => [
            'required' => 'Pertanyaan No 21 bagian perceived harus di isi',
        ],
		
				        'rpm22' => [
            'required' => 'Pertanyaan No 22 bagian minimum harus di isi',
        ],
		'rpd22' => [
            'required' => 'Pertanyaan No 22 bagian desired harus di isi',
        ],
		'rpp22' => [
            'required' => 'Pertanyaan No 22 bagian perceived harus di isi',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
