<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Kuesioner</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
		<link href="{{ asset("/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css" />
		<script src="{{ asset("/jquery/dist/jquery.min.js")}}"></script>
		<script src="{{ asset("/bootstrap/dist/js/bootstrap.min.js")}}"></script>


        <!-- Styles -->
    </head>
    <body>
	<h2 align="center">
        Kuesioner Pertanyaan
    </h2>
	<div class="panel-body">
					<form action="{{ URL::to('savekuesioner') }}" class="form-horizontal" method="post">
					 {{ csrf_field() }}
                    <div class="row">
                       
					<div class="table-responsive col-sm-12">
					
                            <table class="table table-striped table-bordered" id="setting-product-table">
                                <thead>
                                    <tr>
                                        <th style="text-align:center" valign="top" rowspan="4"><div>Question</div></th>
										<th style="width: 10px" rowspan="4"></th>
                                        <th colspan="9" style="text-align:center">My Minimum<br>Service Level Is</th>
										<th style="width: 10px" rowspan="4"></th>
                                        <th colspan="9" style="text-align:center">My Desired<br>Service Level Is</th>
										<th style="width: 10px" rowspan="4"></th>
                                        <th colspan="9" style="text-align:center">My Perceived<br>Service Performance Is</th>
                                    </tr>
									<tr>                                       
                                        <th colspan="9">Low <div style="text-align:right">High</th>
										<th colspan="9">Low <div style="text-align:right">High</div> </th>
										<th colspan="9">Low <div style="text-align:right">High</div> </th>
                                     
                                    </tr>
									<tr>                                       
                                        <th>1</th>
                                        <th>2</th>
										 <th>3</th>
                                        <th>4</th>
										 <th>5</th>
                                        <th>6</th>
										<th>7</th>
										<th>8</th>
										<th>9</th>
										 <th>1</th>
                                        <th>2</th>
										 <th>3</th>
                                        <th>4</th>
										 <th>5</th>
                                        <th>6</th>
										<th>7</th>
										<th>8</th>
										<th>9</th>
										 <th>1</th>
                                        <th>2</th>
										 <th>3</th>
                                        <th>4</th>
										 <th>5</th>
                                        <th>6</th>
										<th>7</th>
										<th>8</th>
										<th>9</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  
									<tr>
										<td>Pustakawan berpakaian sopan dan rapi</td>
										<td></td>
										<div class="form-group {!! $errors->has('rpm1') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpm1' name="rpm1" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm1" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm1" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm1" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm1" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm1" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm1" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm1" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm1" value="9" ></label></td>
										{!! $errors->first('rpm1', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpd1') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpd1' name="rpd1" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd1" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd1" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd1" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd1" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd1" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd1" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd1" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd1" value="9"></label></td>
										{!! $errors->first('rpd1', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpp1') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpp1' name="rpp1" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp1" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp1" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp1" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp1" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp1" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp1" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp1" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp1" value="9"></label></td>
										{!! $errors->first('rpp1', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Pustakawan Bersikap ramah ketika melayani</td>
										<td></td>
										<div class="form-group {!! $errors->has('rpm2') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpm1' name="rpm2" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm2" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm2" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm2" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm2" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm2" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm2" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm2" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm2" value="9" ></label></td>
										{!! $errors->first('rpm2', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpd2') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpd1' name="rpd2" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd2" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd2" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd2" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd2" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd2" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd2" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd2" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd2" value="9"></label></td>
										{!! $errors->first('rpd2', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpp2') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpp1' name="rpp2" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp2" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp2" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp2" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp2" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp2" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp2" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp2" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp2" value="9"></label></td>
										{!! $errors->first('rpp2', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Pustakawan tanggap membantu kesulitan saya</td>
										<td></td>
																				<div class="form-group {!! $errors->has('rpm3') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpm1' name="rpm3" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm3" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm3" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm3" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm3" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm3" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm3" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm3" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm3" value="9" ></label></td>
										{!! $errors->first('rpm3', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpd3') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpd1' name="rpd3" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd3" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd3" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd3" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd3" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd3" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd3" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd3" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd3" value="9"></label></td>
										{!! $errors->first('rpd3', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpp3') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpp1' name="rpp3" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp3" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp3" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp3" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp3" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp3" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp3" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp3" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp3" value="9"></label></td>
										{!! $errors->first('rpp3', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Pustakawan mengerti kebutuhan saya ketika mencari informasi	</td>
										<td></td>
										<div class="form-group {!! $errors->has('rpm4') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpm1' name="rpm4" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm4" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm4" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm4" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm4" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm4" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm4" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm4" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm4" value="9" ></label></td>
										{!! $errors->first('rpm4', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpd4') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpd1' name="rpd4" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd4" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd4" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd4" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd4" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd4" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd4" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd4" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd4" value="9"></label></td>
										{!! $errors->first('rpd4', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpp4') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpp1' name="rpp4" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp4" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp4" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp4" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp4" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp4" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp4" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp4" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp4" value="9"></label></td>
										{!! $errors->first('rpp4', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Pustakawan mampu menjawab pertanyaan yang saya ajukan	</td>
										<td></td>
										<div class="form-group {!! $errors->has('rpm5') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpm1' name="rpm5" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm5" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm5" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm5" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm5" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm5" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm5" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm5" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm5" value="9" ></label></td>
										{!! $errors->first('rpm5', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpd5') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpd1' name="rpd5" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd5" value="2"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd5" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd5" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd5" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd5" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd5" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd5" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd5" value="9"></label></td>
										{!! $errors->first('rpd5', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpp5') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpp1' name="rpp5" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp5" value="2"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp5" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp5" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp5" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp5" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp5" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp5" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp5" value="9"></label></td>
										{!! $errors->first('rpp5', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Pustakawan membimbing saya mencari informasi pada koleksi	</td>
										<td></td>
										<div class="form-group {!! $errors->has('rpm6') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpm1' name="rpm6" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm6" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm6" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm6" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm6" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm6" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm6" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm6" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm6" value="9" ></label></td>
										{!! $errors->first('rpm6', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
																				<div class="form-group {!! $errors->has('rpd6') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpd1' name="rpd6" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd6" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd6" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd6" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd6" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd6" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd6" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd6" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd6" value="9"></label></td>
										{!! $errors->first('rpd6', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
																				<div class="form-group {!! $errors->has('rpp6') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpp1' name="rpp6" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp6" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp6" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp6" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp6" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp6" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp6" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp6" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp6" value="9"></label></td>
										{!! $errors->first('rpp6', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Pustakawan bersikap perhatian dan peduli terhadap kebutuhan informasi saya	</td>
										<td></td>
																														<div class="form-group {!! $errors->has('rpm7') ? 'has-error' : '' !!}">

										<td><label><input type="radio" id='rpm1' name="rpm7" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm7" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm7" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm7" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm7" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm7" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm7" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm7" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm7" value="9" ></label></td>
										{!! $errors->first('rpm7', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
																														<div class="form-group {!! $errors->has('rpd7') ? 'has-error' : '' !!}">

										<td><label><input type="radio" id='rpd1' name="rpd7" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd7" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd7" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd7" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd7" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd7" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd7" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd7" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd7" value="9"></label></td>
										{!! $errors->first('rpd7', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
																														<div class="form-group {!! $errors->has('rpp7') ? 'has-error' : '' !!}">

										<td><label><input type="radio" id='rpp1' name="rpp7" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp7" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp7" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp7" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp7" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp7" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp7" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp7" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp7" value="9"></label></td>
										{!! $errors->first('rpp7', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Kemutakhiran koleksi perpustakaan (up to date)	</td>
										<td></td>
										<div class="form-group {!! $errors->has('rpm8') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpm1' name="rpm8" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm8" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm8" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm8" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm8" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm8" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm8" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm8" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm8" value="9" ></label></td>
										{!! $errors->first('rpm8', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpd8') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpd1' name="rpd8" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd8" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd8" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd8" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd8" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd8" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd8" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd8" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd8" value="9"></label></td>
										{!! $errors->first('rpd8', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpp8') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpp1' name="rpp8" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp8" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp8" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp8" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp8" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp8" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp8" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp8" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp8" value="9"></label></td>
										{!! $errors->first('rpp8', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Koleksi buku di perpustakaan bermanfaat untuk pekerjaan saya	</td>
										<td></td>
										<div class="form-group {!! $errors->has('rpm9') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpm1' name="rpm9" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm9" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm9" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm9" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm9" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm9" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm9" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm9" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm9" value="9" ></label></td>
										{!! $errors->first('rpm9', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpd9') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpd1' name="rpd9" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd9" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd9" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd9" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd9" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd9" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd9" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd9" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd9" value="9"></label></td>
										{!! $errors->first('rpd9', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpp9') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpp1' name="rpp9" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp9" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp9" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp9" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp9" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp9" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp9" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp9" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp9" value="9"></label></td>
										{!! $errors->first('rpp9', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Koleksi Surat Kabar membuat saya mengetahui berita terkini	</td>
										<td></td>
										<div class="form-group {!! $errors->has('rpm10') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpm1' name="rpm10" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm10" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm10" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm10" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm10" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm10" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm10" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm10" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm10" value="9" ></label></td>
										{!! $errors->first('rpm10', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpd10') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpd1' name="rpd10" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd10" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd10" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd10" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd10" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd10" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd10" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd10" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd10" value="9"></label></td>
										{!! $errors->first('rpd10', '<div class="error"><b>:message<b></div>') !!}
										</div>
										<td></td>
										<div class="form-group {!! $errors->has('rpp10') ? 'has-error' : '' !!}">
										<td><label><input type="radio" id='rpp1' name="rpp10" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp10" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp10" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp10" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp10" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp10" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp10" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp10" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp10" value="9"></label></td>
										{!! $errors->first('rpp10', '<div class="error"><b>:message<b></div>') !!}
										</div>
									</tr>
									<tr>
										<td>Komputer di perpustakaan mudah untuk di gunakan	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm11" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm11" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm11" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm11" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm11" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm11" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm11" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm11" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm11" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd11" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd11" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd11" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd11" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd11" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd11" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd11" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd11" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd11" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp11" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp11" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp11" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp11" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp11" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp11" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp11" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp11" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp11" value="9"></label></td>
									</tr>
									<tr>
										<td>Koleksi majalah dapat menghibur dan membantu saya memperoleh informasi	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm12" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm12" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm12" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm12" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm12" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm12" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm12" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm12" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm12" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd12" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd12" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd12" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd12" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd12" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd12" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd12" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd12" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd12" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp12" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp12" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp12" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp12" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp12" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp12" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp12" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp12" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp12" value="9"></label></td>
									</tr>
									<tr>
										<td>Saya mudah menemukan dan menjangkau koleksi di rak	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm13" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm13" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm13" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm13" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm13" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm13" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm13" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm13" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm13" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd13" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd13" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd13" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd13" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd13" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd13" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd13" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd13" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd13" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp13" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp13" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp13" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp13" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp13" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp13" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp13" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp13" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp13" value="9"></label></td>
									</tr>
									<tr>
										<td>Adanya fasilitas Internet di perpustakaan membantu saya memperoleh informasi	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm14" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm14" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm14" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm14" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm14" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm14" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm14" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm14" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm14" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd14" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd14" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd14" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd14" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd14" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd14" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd14" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd14" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd14" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp14" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp14" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp14" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp14" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp14" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp14" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp14" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp14" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp14" value="9"></label></td>
									</tr>
									<tr>
										<td>Waktu operasional perpustakaan sesuai dengan kebutuhan	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm15" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm15" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm15" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm15" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm15" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm15" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm15" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm15" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm15" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd15" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd15" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd15" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd15" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd15" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd15" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd15" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd15" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd15" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp15" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp15" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp15" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp15" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp15" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp15" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp15" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp15" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp15" value="9"></label></td>
									</tr>
									<tr>
										<td>OPAC (Katalog yang berbeda di komputer) mudah dan nyaman di gunakan</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm16" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm16" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm16" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm16" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm16" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm16" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm16" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm16" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm16" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd16" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd16" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd16" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd16" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd16" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd16" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd16" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd16" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd16" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp16" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp16" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp16" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp16" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp16" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp16" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp16" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp16" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp16" value="9"></label></td>
									</tr>
									<tr>
										<td>Suasana di dalam perpustakaan terasa tenang dan kondusif untuk membaca	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm17" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm17" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm17" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm17" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm17" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm17" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm17" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm17" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm17" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd17" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd17" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd17" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd17" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd17" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd17" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd17" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd17" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd17" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp17" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp17" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp17" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp17" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp17" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp17" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp17" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp17" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp17" value="9"></label></td>
									</tr>
									<tr>
										<td>Ruang perpustakaan yang menarik membuat saya berkeinginan untuk mengunjungi perpustakaan	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm18" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm18" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm18" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm18" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm18" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm18" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm18" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm18" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm18" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd18" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd18" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd18" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd18" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd18" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd18" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd18" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd18" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd18" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp18" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp18" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp18" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp18" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp18" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp18" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp18" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp18" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp18" value="9"></label></td>
									</tr>
									<tr>
										<td>Suasana perpustakaan mendukung saya untuk fokus/konsentrasi pada apa yang saya baca	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm19" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm19" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm19" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm19" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm19" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm19" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm19" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm19" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm19" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd19" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd19" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd19" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd19" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd19" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd19" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd19" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd19" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd19" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp19" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp19" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp19" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp19" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp19" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp19" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp19" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp19" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp19" value="9"></label></td>
									</tr>
									<tr>
										<td>Ruang perpustakaan merupakan tempat yang positif untuk saya menghabiskan waktu	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm20" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm20" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm20" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm20" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm20" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm20" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm20" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm20" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm20" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd20" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd20" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd20" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd20" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd20" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd20" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd20" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd20" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd20" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp20" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp20" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp20" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp20" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp20" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp20" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp20" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp20" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp20" value="9"></label></td>
									</tr>
									<tr>
										<td>Ruang Perpustakaan membuat saya menjadi terinspirasi	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm21" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm21" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm21" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm21" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm21" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm21" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm21" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm21" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm21" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd21" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd21" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd21" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd21" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd21" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd21" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd21" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd21" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd21" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp21" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp21" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp21" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp21" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp21" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp21" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp21" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp21" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp21" value="9"></label></td>
									</tr>
									<tr>
										<td>Keadaan fasilitas-fasilitas fisik (kursi, meja baca , dll) di perpustakaan	</td>
										<td></td>
										<td><label><input type="radio" id='rpm1' name="rpm22" value="1" ></label></td>
										<td><label><input type="radio" id='rpm2' name="rpm22" value="2" ></label></td>
										<td><label><input type="radio" id='rpm3' name="rpm22" value="3" ></label></td>
										<td><label><input type="radio" id='rpm4' name="rpm22" value="4" ></label></td>
										<td><label><input type="radio" id='rpm5' name="rpm22" value="5" ></label></td>
										<td><label><input type="radio" id='rpm6' name="rpm22" value="6" ></label></td>
										<td><label><input type="radio" id='rpm7' name="rpm22" value="7" ></label></td>
										<td><label><input type="radio" id='rpm8' name="rpm22" value="8" ></label></td>
										<td><label><input type="radio" id='rpm9' name="rpm22" value="9" ></label></td>
										<td></td>
										<td><label><input type="radio" id='rpd1' name="rpd22" value="1"></label></td>
										<td><label><input type="radio" id='rpd2' name="rpd22" value="2"></label></td>
										<td><label><input type="radio" id='rpd3' name="rpd22" value="3"></label></td>
										<td><label><input type="radio" id='rpd4' name="rpd22" value="4"></label></td>
										<td><label><input type="radio" id='rpd5' name="rpd22" value="5"></label></td>
										<td><label><input type="radio" id='rpd6' name="rpd22" value="6"></label></td>
										<td><label><input type="radio" id='rpd7' name="rpd22" value="7"></label></td>
										<td><label><input type="radio" id='rpd8' name="rpd22" value="8"></label></td>
										<td><label><input type="radio" id='rpd9' name="rpd22" value="9"></label></td>
										<td></td>
										<td><label><input type="radio" id='rpp1' name="rpp22" value="1"></label></td>
										<td><label><input type="radio" id='rpp2' name="rpp22" value="2"></label></td>
										<td><label><input type="radio" id='rpp3' name="rpp22" value="3"></label></td>
										<td><label><input type="radio" id='rpp4' name="rpp22" value="4"></label></td>
										<td><label><input type="radio" id='rpp5' name="rpp22" value="5"></label></td>
										<td><label><input type="radio" id='rpp6' name="rpp22" value="6"></label></td>
										<td><label><input type="radio" id='rpp7' name="rpp22" value="7"></label></td>
										<td><label><input type="radio" id='rpp8' name="rpp22" value="8"></label></td>
										<td><label><input type="radio" id='rpp9' name="rpp22" value="9"></label></td>
									</tr>
                                    </tbody>
                                </table>
								
                            </div>
                        </div>
							<button class="btn btn-sm btn-success" type="submit">
                                <i class="fa fa-plus"></i>
                                &nbsp; Submit
                            </button>
					</form>
			 </div>
							
        
    </body>
</html>
