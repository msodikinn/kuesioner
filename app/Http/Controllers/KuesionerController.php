<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Soal;
use App\Kuesioner;
use DB;

class KuesionerController extends Controller
{
	protected $master = array();

	public function __construct (array $master = array()) {
		$this->master = $master;
		//$this->middleware('auth');
	}
	
	
        public function index()
    {
        $soals = Soal::all();
		//var_dump($soals);exit;
		return view('welcome', compact('soals'));
    }
	
		public function saveKuesioner(Request $request)
	{
		  $this->validate($request, [
            'rpm1' => 'required','rpd1' => 'required','rpp1' => 'required',
			'rpm2' => 'required','rpd2' => 'required','rpp2' => 'required',
			'rpm3' => 'required','rpd3' => 'required','rpp3' => 'required',
			'rpm4' => 'required','rpd4' => 'required','rpp4' => 'required',
			'rpm5' => 'required','rpd5' => 'required','rpp5' => 'required',
			'rpm6' => 'required','rpd6' => 'required','rpp6' => 'required',
			'rpm7' => 'required','rpd7' => 'required','rpp7' => 'required',
			'rpm8' => 'required','rpd8' => 'required','rpp8' => 'required',
			'rpm9' => 'required','rpd9' => 'required','rpp9' => 'required',
			'rpm10' => 'required','rpd10' => 'required','rpp10' => 'required'
        ]); 
		
		 $data1 = new Kuesioner;
		 $minimum = $request->input('rpm1');
		 $data1->soal = "P1";
		 $data1->minimum = $minimum;
		 $desired = $request->input('rpd1');
		 $data1->desired = $desired;
		 $perceived = $request->input('rpp1');
		 $data1->perceived = $perceived; 
		 
		 $data2 = new Kuesioner;
		 $minimum2 = $request->input('rpm2');
		 $data2->soal = "P2";
		 $data2->minimum = $minimum2;
		 $desired2 = $request->input('rpd2');
		 $data2->desired = $desired2;
		 $perceived2 = $request->input('rpp2');
		 $data2->perceived = $perceived2;
		 
		 $data3 = new Kuesioner;
		 $minimum3 = $request->input('rpm3');
		 $data3->soal = "P3";
		 $data3->minimum = $minimum3;
		 $desired3 = $request->input('rpd3');
		 $data3->desired = $desired3;
		 $perceived3 = $request->input('rpp3');
		 $data3->perceived = $perceived3;
		 
		 $data4 = new Kuesioner;
		 $minimum4 = $request->input('rpm4');
		 $data4->soal = "P4";
		 $data4->minimum = $minimum4;
		 $desired4 = $request->input('rpd4');
		 $data4->desired = $desired4;
		 $perceived4 = $request->input('rpp4');
		 $data4->perceived = $perceived4;
		 
		  $data5 = new Kuesioner;
		 $minimum5 = $request->input('rpm5');
		 $data5->soal = "P5";
		 $data5->minimum = $minimum5;
		 $desired5 = $request->input('rpd5');
		 $data5->desired = $desired5;
		 $perceived5 = $request->input('rpp5');
		 $data5->perceived = $perceived5;
		 
		  $data6 = new Kuesioner;
		 $minimum6 = $request->input('rpm6');
		 $data6->soal = "P6";
		 $data6->minimum = $minimum6;
		 $desired6 = $request->input('rpd6');
		 $data6->desired = $desired6;
		 $perceived6 = $request->input('rpp6');
		 $data6->perceived = $perceived6;
		 
		  $data7 = new Kuesioner;
		 $minimum7 = $request->input('rpm7');
		 $data7->soal = "P7";
		 $data7->minimum = $minimum7;
		 $desired7 = $request->input('rpd7');
		 $data7->desired = $desired7;
		 $perceived7 = $request->input('rpp7');
		 $data7->perceived = $perceived7;
		 
		  $data8 = new Kuesioner;
		 $minimum8 = $request->input('rpm8');
		 $data8->soal = "P8";
		 $data8->minimum = $minimum8;
		 $desired8 = $request->input('rpd8');
		 $data8->desired = $desired8;
		 $perceived8 = $request->input('rpp8');
		 $data8->perceived = $perceived8;
		 
		  $data9 = new Kuesioner;
		 $minimum9 = $request->input('rpm9');
		 $data9->soal = "P9";
		 $data9->minimum = $minimum9;
		 $desired9 = $request->input('rpd9');
		 $data9->desired = $desired9;
		 $perceived9 = $request->input('rpp9');
		 $data9->perceived = $perceived9;
		 
		  $data10 = new Kuesioner;
		 $minimum10 = $request->input('rpm10');
		 $data10->soal = "P10";
		 $data10->minimum = $minimum10;
		 $desired10 = $request->input('rpd10');
		 $data10->desired = $desired10;
		 $perceived10 = $request->input('rpp10');
		 $data10->perceived = $perceived10;
		 
		  $data11 = new Kuesioner;
		 $minimum11 = $request->input('rpm11');
		 $data11->soal = "P11";
		 $data11->minimum = $minimum11;
		 $desired11 = $request->input('rpd11');
		 $data11->desired = $desired11;
		 $perceived11 = $request->input('rpp11');
		 $data11->perceived = $perceived11;
		 
		  $data12 = new Kuesioner;
		 $minimum12 = $request->input('rpm12');
		 $data12->soal = "P12";
		 $data12->minimum = $minimum12;
		 $desired12 = $request->input('rpd12');
		 $data12->desired = $desired12;
		 $perceived12 = $request->input('rpp12');
		 $data12->perceived = $perceived12;
		 
		  $data13 = new Kuesioner;
		 $minimum13 = $request->input('rpm13');
		 $data13->soal = "P13";
		 $data13->minimum = $minimum13;
		 $desired13 = $request->input('rpd13');
		 $data13->desired = $desired13;
		 $perceived13 = $request->input('rpp13');
		 $data13->perceived = $perceived13;
		 
		  $data14 = new Kuesioner;
		 $minimum14 = $request->input('rpm14');
		 $data14->soal = "P14";
		 $data14->minimum = $minimum14;
		 $desired14 = $request->input('rpd14');
		 $data14->desired = $desired14;
		 $perceived14 = $request->input('rpp14');
		 $data14->perceived = $perceived14;
		 
		  $data15 = new Kuesioner;
		 $minimum15 = $request->input('rpm15');
		 $data15->soal = "P15";
		 $data15->minimum = $minimum15;
		 $desired15 = $request->input('rpd15');
		 $data15->desired = $desired15;
		 $perceived15 = $request->input('rpp15');
		 $data15->perceived = $perceived15;
		 
		 $data16 = new Kuesioner;
		 $minimum16 = $request->input('rpm16');
		 $data16->soal = "P16";
		 $data16->minimum = $minimum16;
		 $desired16 = $request->input('rpd16');
		 $data16->desired = $desired16;
		 $perceived16 = $request->input('rpp16');
		 $data16->perceived = $perceived16;
		 
		  $data17 = new Kuesioner;
		 $minimum17 = $request->input('rpm17');
		 $data17->soal = "P17";
		 $data17->minimum = $minimum17;
		 $desired17 = $request->input('rpd17');
		 $data17->desired = $desired17;
		 $perceived17 = $request->input('rpp17');
		 $data17->perceived = $perceived17;
		 
		 $data18 = new Kuesioner;
		 $minimum18 = $request->input('rpm18');
		 $data18->soal = "P18";
		 $data18->minimum = $minimum18;
		 $desired18 = $request->input('rpd18');
		 $data18->desired = $desired18;
		 $perceived18 = $request->input('rpp18');
		 $data18->perceived = $perceived18;
		 
		 $data19 = new Kuesioner;
		 $minimum19 = $request->input('rpm19');
		 $data19->soal = "P19";
		 $data19->minimum = $minimum19;
		 $desired19 = $request->input('rpd19');
		 $data19->desired = $desired19;
		 $perceived19 = $request->input('rpp19');
		 $data19->perceived = $perceived19;
		 
		 $data20 = new Kuesioner;
		 $minimum20 = $request->input('rpm20');
		 $data20->soal = "P20";
		 $data20->minimum = $minimum20;
		 $desired20 = $request->input('rpd20');
		 $data20->desired = $desired20;
		 $perceived20 = $request->input('rpp20');
		 $data20->perceived = $perceived20;
		 
		  $data21 = new Kuesioner;
		 $minimum21 = $request->input('rpm21');
		 $data21->soal = "P21";
		 $data21->minimum = $minimum21;
		 $desired21 = $request->input('rpd21');
		 $data21->desired = $desired21;
		 $perceived21 = $request->input('rpp21');
		 $data21->perceived = $perceived21;
		 
		  $data22 = new Kuesioner;
		 $minimum22 = $request->input('rpm22');
		 $data22->soal = "P22";
		 $data22->minimum = $minimum22;
		 $desired22 = $request->input('rpd22');
		 $data22->desired = $desired22;
		 $perceived22 = $request->input('rpp22');
		 $data22->perceived = $perceived22;
		 
		  $collection = new Collection;
		 $collection->push($data1);
		/*  $collection->push($data2);
		 $collection->push($data3);
		 $collection->push($data4);
		 $collection->push($data5);
		 $collection->push($data6);
		 $collection->push($data7);
		 $collection->push($data8);
		 $collection->push($data9);
		 $collection->push($data10);
		 $collection->push($data11);
		$collection->push($data12);
		$collection->push($data13);
		$collection->push($data14);
		$collection->push($data15);
		$collection->push($data16);
		$collection->push($data17);
		$collection->push($data18);
		$collection->push($data19);
		$collection->push($data20);
		$collection->push($data21);
		$collection->push($data22); */
		
		 DB::table('kuesioner')->insert($collection->toArray());
		 
		 //$data1->save();
	}
	
}
